# Snake

A simple snake game

## Functionality

- Objective:
    - The player controls a snake that moves around the screen.
    - The goal is to eat food items (represented by dots) to grow longer.

- Gameplay:
    - The snake moves continuously in the direction it was last directed.
    - The player can change the direction of the snake using arrow keys or other specified controls.

- Growing Length:
    - When the snake consumes a food item, it grows longer.
    - The player scores points for each food item eaten.

- Collision:
    - If the snake collides with the screen border or itself, the game ends.
    - The challenge increases as the snake gets longer, making it more difficult to avoid collisions.

- Score:
    - The player's score is based on the number of food items eaten.

- Game Over:
    - When the snake collides, the game displays the player's final score and may offer the option to play again.


## Project Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:

- Windows:
    1. Fork the repository
    2. Clone the repository down to your local machine
    3. CD into the new project directory
    4. Run `python -m venv .venv`
    5. Run `.\.venv\Scripts\Activate.ps1`
    6. Run `python.exe -m pip install --upgrade pip`
    7. Run `pip install -r requirements.txt`
    8. Run `python main.py`
    9. Play!

- Mac:
    1. Fork the repository
    2. Clone the repository down to your local machine
    3. CD into the new project directory
    4. Run `python3 -m venv .venv`
    5. Run `source .venv/bin/activate`
    6. Run `python.exe -m pip install --upgrade pip`
    7. Run `pip install -r requirements.txt`
    8. Run `python main.py`
    9. Play!
